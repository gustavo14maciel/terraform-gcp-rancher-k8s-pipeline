![project_image](https://gitlab.com/gustavo14maciel/terraform-rancher-k8s-pipeline/uploads/a198fd6e6f90398c3ec83068760ac848/project_image.png)

# Kubernetes com Rancher na GCP usando Terraform

## Terraform

O primeiro passo é subir a infraestrutura necessária (máquinas, rede, sub-rede, regras de firewall, dns_zone, etc...) para rodar o Rancher e cluster Kubernetes. Para melhor manutenção da Infra, o Terraform permite modularizar esses componentes.

Como estou usando a GCP, configurei o backend remoto para salvar o state do Terraform no Cloud Storage.

Para rodar os comandos do Terraform usei impersonate para evitar usar uma chave para service account. Mais informações: https://cloud.google.com/blog/topics/developers-practitioners/using-google-cloud-service-account-impersonation-your-terraform-code

Cada módulo tem suas próprias variáveis, eles são rodados usando o comando especificando o arquivo tfvars.
Exemplo: `terraform plan -var-file=./dev/terraform.tfvars`

**Módulos**:

- Ansible:
  Módulo para gerar arquivo de configuração do Ansible com os IPs necessários.

- DNS:
  Criação da zona de DNS e os record sets do Rancher e um Wildcard '\*' para as aplicações.

- Kubernetes:
  Provisionamento dos nodes do cluster.

- Network:
  Criação da VPC, das Subnets e das regras de firewall necessárias para liberar as portas do Rancher e Kubernetes. Para mais informações: https://rancher.com/docs/rancher/v2.5/en/installation/requirements/ports/#ports-for-rancher-server-nodes-on-rke. O Rancher está sendo criado numa subnet diferente da usada no Kubernetes, pois o free tier da GCP apenas permite 4 IPs estáticos em uma das regiões usadas.

- Rancher:
  Provisionamento das 3 máquinas do Rancher, da máquina do NGINX que vai subir o Rancher em High Availability (HA), do arquivo de configuração do RKE e do arquivo de configuração do NGINX.

Imagem das máquinas: Ubuntu 16.04.7 LTS

## Ansible

Todas as máquinas precisam do Docker instalado. Para automatizar a instalação nas máquinas utilizei o Ansible. Para usar o Ansible é preciso: ter um host controlador (o que vai executar os comandos nos outros servers), o Ansible instalado nesse host controlador e o Python instalado nas máquinas que vão receber comandos do Ansible.

Além disso, é necessário que o Ansible consiga fazer SSH em cada máquina. Para isso é preciso adicionar a chave SSH pública do controlador nos hosts.

O Ansible precisa dos IPs das máquinas em um arquivo chamado 'hosts' para poder fazer o SSH. Com o output do Terraform eu consigo usar um template .tpl para pegar os IPs das máquinas na hora de rodar o terraform apply usando o data source 'template_file' https://registry.terraform.io/providers/hashicorp/template/latest/docs/data-sources/file. Para criar o arquivo em si, pode ser usado o resource 'local_file' com o template renderizado https://registry.terraform.io/providers/hashicorp/local/latest/docs/resources/file.

Por fim, basta executar `ansible-playbook playbook.yml -u meu_usuario`

## Rancher

O Rancher é uma plataforma open source que facilita o gerenciamento de múltiplos clusters Kubernetes. https://rancher.com/.

Ele permite a instalação em Single Node ou HA usando o próprio Kubernetes.

Para subir o Rancher dentro do Kubernetes estou usando o Rancher Kubernetes Engine (RKE) que é uma distribuição do Kubernetes que roda dentro de containers do Docker. https://rancher.com/docs/rke/latest/en/installation/

É necessário também instalar o **Helm** e **kubectl**.

O RKE usa um arquivo de configuração **rancher-cluster.yml** para definir os nodes, seus roles e também configurações adicionais. Assim como o Ansible, o RKE precisa ter meios de fazer SSH nas máquinas para subir o cluster. Além disso, o usuário que o RKE vai usar precisa ter permissões para rodar comandos do Docker na máquina. https://www.rancher.co.jp/docs/rancher/v2.x/en/installation/ha/kubernetes-rke/#create-the-rancher-cluster-yml-file

Comando para subir o cluster: `rke up --config ./rancher-cluster.yml`

Quando o cluster terminar de subir o arquivo **kube_config_rancher-cluster.yml** será gerado e ele contém as credenciais para poder usar o kubectl no cluster.

Para configurar as credenciais, basta setar a variável de ambiente KUBECONFIG para: `KUBECONFIG=kube_config_rancher-cluster.yml` ou colocar o conteúdo do arquivo em '~/.kube/config'.

Com o acesso ao cluster, rodar o NGINX em um container do Docker, adicionar o repo do Rancher, criar o namespace cattle system, configurar o cert-manager e instalar o Rancher com o Helm (**como no arquivo rancher_install.sh no repositório**).

Por fim, acessar o DNS do Rancher que está apontando para o NGINX (e configurado para mandar o tráfego para as máquinas do cluster, especificado no arquivo **nginx.conf**) e acessar a UI do Rancher.

## Kubernetes

O ideal em um cluster Kubernetes em HA é que as máquinas sejam robustas, estou rodando em menos máquinas e máquinas menores por causa das quotas do GCP (restrições de quanto recurso posso usar). https://cloud.google.com/docs/quota

Recomendações de hardware etcd: https://etcd.io/docs/v3.3/op-guide/hardware/

Kubernetes HA: https://rancher.com/learning-paths/building-a-highly-available-kubernetes-cluster/

Uma possível arquitetura seria:

- 3 Máquinas etcd
- 2 Máquinas controlplane
- 3-4 Máquinas workers

Arquitetura atual:

- 2 Máquinas etcd + controlplane
- 1 Máquina worker + etcd
- 1 Máquina worker

Para subir o cluster Kubernetes pelo Rancher, basta ir na aba dos clusters e clicar em 'Add Cluster'. Nessa opção é possível criar um novo cluster ou importar seja usando máquinas ou um serviço de Kubernetes (EKS, AKS, GKE). Eu criei um cluster custom (usando as máquinas criadas com o Terraform).

Existem várias opções de alteração na interface do Rancher como: nome do cluster, versão do Kubernetes, roles de membros, registry privado, habilitar ingress do NGINX, etc...

Após isso, o Rancher vai gerar um `docker run` para rodar nos nodes que serão adicionados ao cluster, podendo especificar o nome e a role.

Exemplo:
`sudo docker run -d --privileged --restart=unless-stopped --net=host -v /etc/kubernetes:/etc/kubernetes -v /var/run:/var/run rancher/rancher-agent:v2.5.6 --server https://rancher.gustavoamaciel.com --token h12ncs6z4sj2752abch3181qdrx3j1lwpvglk18af27ffaj7sybb1a --ca-checksum ja86dfv927ffaj7s4dbefbd8aikmiqjwe78123h316fgm14b2b50d45b65d7ht42 --node-name etcd-controlplane-0 --etcd --controlplane`

O provisionamento leva em torno de 10 minutos. Quando terminar, o cluster estará pronto e disponível pela interface do Rancher e pelo kubectl.

Para acessar o cluster importado pro Rancher pelo kubectl, é possível baixar o Kubeconfig: https://rancher.com/docs/rancher/v2.5/en/cluster-admin/cluster-access/kubectl/

Usar imagens de registry privado no Kubernetes: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/#create-a-secret-by-providing-credentials-on-the-command-line

---

# CI/CD Gitlab

Com a Infra toda provisionada desde as máquinas, redes, DNS, Kubernetes e Rancher é hora de colocar as aplicações para rodar.

O arquivo **.gitlab-ci.yml** vai descrever todas as instruções da Pipeline para os dois simples microserviços (NodeJS e Golang) que vão ser deployados no cluster Kubernetes.

O Gitlab utiliza Runners, aplicações que rodam os Jobs da Pipeline. A própria plataforma fornece Runners compartilhados, mas eles têm um limite em quanto podem ser usados. https://docs.gitlab.com/ee/ci/runners/runners_scope.html#shared-runners

Existe a opção de instalar um Runner em um ambiente próprio seja pelo Docker, Linux, Windows, macOS, Kubernetes, etc...https://docs.gitlab.com/runner/install/

Como estou usando Kubernetes, vou instalar o Runner com o Helm: https://docs.gitlab.com/runner/install/kubernetes.html

Configurações adicionais: https://docs.gitlab.com/runner/install/kubernetes.html#configuring-gitlab-runner-using-the-helm-chart

Cada Pipeline tem suas particularidades, no meu caso eu tenho 4 Stages (etapas):

- test
- build
- push
- deploy

## Test

Etapa que instala as dependências da aplicação e executa os testes. Os testes podem ser desde testes unitários a testes funcionais ou até testes de qualidade de código.

## Build

As aplicações são feitas para rodar em containers, essa etapa vai fazer o build da imagem Docker usando o Dockerfile que está na pasta de cada aplicação. Para rodar comandos do Docker na Pipeline é preciso fazer configurações adicionais no Runner. Uma das formas é usar o **dind** (Docker-in-Docker). https://docs.gitlab.com/ee/ci/docker/using_docker_build.html

Docker-in-Docker com Kubernetes: https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#docker-in-docker-with-tls-enabled-in-kubernetes

Nesse stage estou usando uma variável predefinida do Gitlab, que pega os 8 primeiros dígitos do commit, para gerar a tag da imagem: `CI_COMMIT_SHORT_SHA`. https://docs.gitlab.com/ee/ci/variables/predefined_variables.html

Para manter uma variável secreta, é possível adicionar variáveis no escopo do projeto. Essas variáveis podem ser: **Protected** que são variáveis apenas expostas em branches protegidas e tags. **Masked** que é uma opção de esconder a variável nos logs do Job.
Exemplo: `DOCKER_HUB_REPO` que é o meu usuário + repo do Docker Hub.

Adicionar uma variável a um projeto: https://docs.gitlab.com/ee/ci/variables/index.html#add-a-cicd-variable-to-a-project

Como vou precisar usar a imagem no Stage de Push, vou usar o comando `docker save` para salvá-la em um arquivo .tar e a keyword **artifacts** para dizer ao Gitlab o que salvar depois de executar o Job.

## Push

Esse Stage também usa comandos do Docker e por isso precisa do **dind**. O comando `docker push` vai usar um repositório privado, por isso antes de executar o script é necessário fazer login com `docker login`. O Gitlab tem a keyword **before_script** que, como o nome diz, executa algo antes do script.

Dado que vou fazer o `docker load` de um arquivo de outro Stage, posso usar a keyword **dependencies** para listar os Jobs que vou fazer uso dos artifacts. https://docs.gitlab.com/ee/ci/yaml/#dependencies

## Deploy

Esse é o Stage que vai automaticamente atualizar os manifestos do Kubernetes. Para usar o `kubectl` é necessário ter as credenciais do cluster na Pipeline. Posso usar uma variável secreta com o conteúdo do kubeconfig do cluster em base64 e gerar o arquivo **config** com o comando de decode `echo $KUBE_CREDENTIALS | base64 -d > config`.

Para mudar dinamicamente a imagem do deployment, é possível usar o comando `sed` do Linux para trocar a versão pela criada a partir da Pipeline.

Por fim, aplicar as mudanças com o kubectl.
