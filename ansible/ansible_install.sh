REQUIRED_PKG="ansible"
PKG_OK=$(dpkg-query -W --showformat='${Status}\n' $REQUIRED_PKG|grep "install ok installed")
echo Checking for $REQUIRED_PKG: $PKG_OK
if [ "" = "$PKG_OK" ]; then
  echo "No $REQUIRED_PKG. Setting up $REQUIRED_PKG."
  sudo apt-get update
  sudo apt-get --yes install $REQUIRED_PKG
  dpkg-query -W $REQUIRED_PKG
fi

#chmod 600 ~/.ssh/id_ed25519; chmod 600 ~/.ssh/id_ed25519.pub
# echo <public_key> > .ssh/authorized_keys
# ansible all -m ping
# ansible-playbook playbook.yml
# ssh-keygen -t ed25519 -a 100

