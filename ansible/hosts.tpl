[servers]

rancher0 ansible_host=${rancher_ip_0}
rancher1 ansible_host=${rancher_ip_1}
rancher2 ansible_host=${rancher_ip_2}
nginx ansible_host=${nginx_ip}
etcd-controlplane-0 ansible_host=${kubernetes_ip_0}
etcd-controlplane-1 ansible_host=${kubernetes_ip_1}
worker-etcd-0 ansible_host=${kubernetes_ip_2}
worker-0 ansible_host=${kubernetes_ip_3}

