package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

type Message struct {
	Message string `json:"message"`
}

func version(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Version 4.0")
}

func helloWorld(w http.ResponseWriter, r *http.Request) {
	message := Message{}
	url := os.Getenv("NODE_API_URL")

	if url == "" {
		fmt.Fprintf(w, "NODE_API_URL not set")
		return
	}

	endpoint := fmt.Sprintf("%s/world", url)

	resp, err := http.Get(endpoint)

	if err != nil {
		fmt.Fprintf(w, "Can't reach url")
		return
	}

	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		log.Fatalln(err)
	}

	err = json.Unmarshal(body, &message)

	if err != nil {
		log.Fatalln(err)
	}

	fmt.Fprintf(w, "Hello %s\n", message.Message)
}

func sendHello(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello")
}

func handleRequests() {
	port := 9000
	http.HandleFunc("/helloworld", helloWorld)
	http.HandleFunc("/hello", sendHello)
	http.HandleFunc("/", version)

	log.Println(fmt.Sprintf("Listening on port: %d", port))
	log.Fatal(http.ListenAndServe(fmt.Sprintf("0.0.0.0:%d", port), nil))
}

func main() {
	handleRequests()
}
