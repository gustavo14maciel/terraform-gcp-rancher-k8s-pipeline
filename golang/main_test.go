package main

import "testing"

func TestHelloWorldLen(t *testing.T) {
	tests := []struct {
		name string
		want int
	}{{
		name: "Hello World Len",
		want: 11,
	}}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			helloWorld := "Hello World"
			got := len(helloWorld)

			if got != tt.want {
				t.Errorf("got %d, wanted %d", got, tt.want)
			}
		})
	}
}

func TestHelloWorldString(t *testing.T) {
	tests := []struct {
		name string
		want string
	}{{
		name: "Hello World String",
		want: "Hello World",
	}}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			helloWorld := "Hello World"

			if helloWorld != tt.want {
				t.Errorf("got %s, wanted %s", helloWorld, tt.want)
			}
		})
	}
}
