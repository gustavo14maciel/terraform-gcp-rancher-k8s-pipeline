import express, { Application, Request, Response } from "express";
import axios from "axios";

const app: Application = express();
const port = 3000;

// Body parsing Middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get("/", async (req: Request, res: Response): Promise<Response> => {
  return res.status(200).send("Version 3.0");
});

app.get("/world", async (req: Request, res: Response): Promise<Response> => {
  return res.status(200).send({
    message: "World",
  });
});

app.get(
  "/helloworld",
  async (req: Request, res: Response): Promise<Response> => {
    try {
      let url = process.env.GOLANG_API_URL || "";

      if (url == "") {
        throw new Error("GOLANG_API_URL not set");
      }

      console.log("Making request to: " + url);
      const { data } = await axios.get(url + "/hello");

      return res.status(200).send(data + " World");
    } catch (error) {
      if (error instanceof Error) {
        return res.status(400).send(error.message);
      } else {
        return res.status(400).send(error);
      }
    }
  }
);

try {
  app.listen(port, (): void => {
    console.log(`Connected successfully on port ${port}`);
  });
} catch (error: any) {
  console.error(`Error occured: ${error.message}`);
}
