describe("hello", function () {
  it("add", function () {
    let str = "Hello World";
    expect(str).toBe("Hello World");
  });
});

// World
describe("world", function () {
  it("add", function () {
    let str = "World Hello";
    expect(str).toBe("World Hello");
  });
});
