# INSTALL KUBECTL
REQUIRED="kubectl"
OK=$(ls /usr/local/bin/kubectl && echo yes || echo no)
echo Checking for $REQUIRED: $OK
if [ "no" = "$OK" ]; then
  echo "Kubectl not found"
  sudo apt-get update
  sudo curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
  sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
  kubectl version --client
fi

# INSTALL RKE
REQUIRED="rke"
OK=$(ls /usr/local/bin/rke && echo yes || echo no)
echo Checking for $REQUIRED: $OK
if [ "no" = "$OK" ]; then
  echo "RKE not found"
  sudo curl -LO https://github.com/rancher/rke/releases/download/v1.3.3/rke_linux-amd64
  sudo mv rke_linux-amd64 rke
  sudo chmod +x rke
  sudo mv ./rke /usr/local/bin/rke
  rke --version
fi

#INSTALL HELM
REQUIRED="helm"
OK=$(ls /usr/local/bin/helm && echo yes || echo no)
echo Checking for $REQUIRED: $OK
if [ "no" = "$OK" ]; then
  echo "Helm not found"
  sudo curl -LO https://get.helm.sh/helm-v3.7.2-linux-amd64.tar.gz
  sudo tar -zxvf helm-v3.7.2-linux-amd64.tar.gz
  sudo mv linux-amd64/helm /usr/local/bin/helm
  helm version
fi
