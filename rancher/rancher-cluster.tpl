nodes:
  - address: ${node_0_ip}
    internal_address: ${node_0_internal_ip}
    user: ${user}
    role: [controlplane, worker, etcd]
  - address: ${node_1_ip}
    internal_address: ${node_1_internal_ip}
    user: ${user}
    role: [controlplane, worker, etcd]
  - address: ${node_2_ip}
    internal_address: ${node_2_internal_ip}
    user: ${user}
    role: [controlplane, worker, etcd]

services:
  etcd:
    snapshot: true
    creation: 6h
    retention: 24h

ssh_key_path: ~/.ssh/id_ed25519

ignore_docker_version: true
