# Run NGINX as Docker Container
docker run -d --restart=unless-stopped \
  -p 80:80 -p 443:443 \
  -v /etc/nginx.conf:/etc/nginx/nginx.conf \
  nginx:1.14

# Add Rancher repo
helm repo add rancher-stable https://releases.rancher.com/server-charts/stable
kubectl create namespace cattle-system

# Rancher relies on cert-manager to issue certificates from Rancher’s own generated CA or to request Let’s Encrypt certificates.
kubectl create namespace cert-manager
helm repo add jetstack https://charts.jetstack.io
helm repo update
helm install \
  cert-manager jetstack/cert-manager \
  --namespace cert-manager \
  --version v0.16.1 \
  --set installCRDs=true
kubectl get pods --namespace cert-manager

# Install Rancher with Let's Encrypt
helm install rancher rancher-stable/rancher \
  --namespace cattle-system \
  --set hostname=rancher.gustavoamaciel.com \
  --set ingress.tls.source=letsEncrypt \
  --set letsEncrypt.email=gutavo14maciel@gmail.com

# Verify Rancher rollout
kubectl -n cattle-system rollout status deploy/rancher
