module "network" {
  source                       = "./modules/network"
  vpc_name                     = var.vpc_name
  rancher_subnet_name          = var.rancher_subnet_name
  kubernetes_subnet_name       = var.kubernetes_subnet_name
  rancher_subnet_cidr_range    = var.rancher_subnet_cidr_range
  kubernetes_subnet_cidr_range = var.kubernetes_subnet_cidr_range
  firewall_ingress_name        = var.firewall_ingress_name
  udp_ingress_ports            = var.udp_ingress_ports
  tcp_ingress_ports            = var.tcp_ingress_ports
  target_tags_ingress          = var.target_tags_ingress
  firewall_egress_name         = var.firewall_egress_name
  target_tags_egress           = var.target_tags_egress
  static_ips_name              = var.static_ips_name
}

module "rancher" {
  source                = "./modules/rancher"
  rancher_instance_name = var.rancher_instance_name
  machine_type          = var.machine_type
  subnet                = var.rancher_subnet_name
  external_ips          = module.network.ips_rancher
  tags                  = [var.target_tags_ingress[0], var.target_tags_egress[0]]
  region                = var.region
  hosts                 = module.network.ips_rancher
}

module "kubernetes" {
  source                          = "./modules/kubernetes"
  etcd_controlplane_instance_name = var.etcd_controlplane_instance_name
  etcd_controlplane_machine_type  = var.etcd_controlplane_machine_type
  worker_etcd_instance_name       = var.worker_etcd_instance_name
  worker_etcd_machine_type        = var.worker_etcd_machine_type
  worker_instance_name            = var.worker_instance_name
  worker_machine_type             = var.worker_machine_type
  region                          = var.kubernetes_region
  subnet                          = var.kubernetes_subnet_name
  external_ips                    = module.network.ips_kubernetes
  tags                            = [var.target_tags_ingress[0], var.target_tags_egress[0]]
}

module "ansible" {
  source           = "./modules/ansible"
  hosts            = module.network.ips_rancher
  kubernetes_hosts = module.network.ips_kubernetes
  depends_on = [
    module.rancher,
    module.kubernetes
  ]
}

module "dns" {
  source        = "./modules/dns"
  dns_zone_name = var.dns_zone_name
  dns_name      = var.dns_name
  instance_ip   = var.instance_ip
  instances_ip  = module.network.ips_kubernetes
}


