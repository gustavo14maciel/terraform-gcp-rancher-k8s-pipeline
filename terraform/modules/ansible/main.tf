data "template_file" "hosts" {
  template = file("../ansible/hosts.tpl")
  vars = {
    rancher_ip_0    = var.hosts[0]
    rancher_ip_1    = var.hosts[1]
    rancher_ip_2    = var.hosts[2]
    nginx_ip        = var.hosts[3]
    kubernetes_ip_0 = var.kubernetes_hosts[0]
    kubernetes_ip_1 = var.kubernetes_hosts[1]
    kubernetes_ip_2 = var.kubernetes_hosts[2]
    kubernetes_ip_3 = var.kubernetes_hosts[3]
  }
}

resource "local_file" "hosts" {
  content  = data.template_file.hosts.rendered
  filename = "../ansible/hosts"
}
