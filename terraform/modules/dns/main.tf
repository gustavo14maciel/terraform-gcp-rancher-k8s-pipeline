resource "google_dns_managed_zone" "rancher_dns_zone" {
  name     = var.dns_zone_name
  dns_name = var.dns_name
}

resource "google_dns_record_set" "rancher_nginx_dns" {
  name = "rancher.${google_dns_managed_zone.rancher_dns_zone.dns_name}"
  type = "A"
  ttl  = 300

  managed_zone = google_dns_managed_zone.rancher_dns_zone.name

  rrdatas = [var.instance_ip]
}

resource "google_dns_record_set" "rancher_nginx_dns_wildcard" {
  name = "*.rancher.${google_dns_managed_zone.rancher_dns_zone.dns_name}"
  type = "A"
  ttl  = 300

  managed_zone = google_dns_managed_zone.rancher_dns_zone.name

  rrdatas = var.instances_ip
}
