output "name_servers" {
  value = google_dns_managed_zone.rancher_dns_zone.*.name_servers
}
