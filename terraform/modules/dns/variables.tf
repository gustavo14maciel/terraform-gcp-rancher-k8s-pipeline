variable "dns_zone_name" {
  type = string
}
variable "dns_name" {
  type = string
}
variable "instance_ip" {
  type = string
}
variable "instances_ip" {
  type = list(string)
}
