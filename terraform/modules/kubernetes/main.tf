data "google_compute_image" "image" {
  family  = "ubuntu-pro-1604-lts"
  project = "ubuntu-os-pro-cloud"
}

data "google_service_account" "compute_engine_default_service_account" {
  account_id = "594525568521-compute@developer.gserviceaccount.com"
}

resource "google_compute_instance" "etcd_controlplane_instances" {
  count        = 2
  name         = "${var.etcd_controlplane_instance_name}-${count.index}"
  machine_type = var.etcd_controlplane_machine_type
  zone         = "${var.region}-${var.zones[count.index]}"

  boot_disk {
    initialize_params {
      image = data.google_compute_image.image.self_link
    }
  }

  network_interface {
    subnetwork = var.subnet

    access_config {
      nat_ip = var.external_ips[count.index]
    }
  }

  tags = var.tags

  service_account {
    email  = data.google_service_account.compute_engine_default_service_account.email
    scopes = ["cloud-platform"]
  }
}

resource "google_compute_instance" "worker_etcd_instance" {
  name         = "${var.worker_etcd_instance_name}-0"
  machine_type = var.worker_etcd_machine_type
  zone         = "${var.region}-${var.zones[0]}"

  boot_disk {
    initialize_params {
      image = data.google_compute_image.image.self_link
    }
  }

  network_interface {
    subnetwork = var.subnet

    access_config {
      nat_ip = var.external_ips[2]
    }
  }

  tags = var.tags

  service_account {
    email  = data.google_service_account.compute_engine_default_service_account.email
    scopes = ["cloud-platform"]
  }
}

resource "google_compute_instance" "worker_instance" {
  name         = "${var.worker_instance_name}-0"
  machine_type = var.worker_machine_type
  zone         = "${var.region}-${var.zones[1]}"

  boot_disk {
    initialize_params {
      image = data.google_compute_image.image.self_link
    }
  }

  network_interface {
    subnetwork = var.subnet

    access_config {
      nat_ip = var.external_ips[3]
    }
  }

  tags = var.tags

  service_account {
    email  = data.google_service_account.compute_engine_default_service_account.email
    scopes = ["cloud-platform"]
  }
}

