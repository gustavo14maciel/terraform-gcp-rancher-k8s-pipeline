variable "etcd_controlplane_instance_name" {
  type = string
}

variable "etcd_controlplane_machine_type" {
  type = string
}

variable "worker_etcd_instance_name" {
  type = string
}

variable "worker_etcd_machine_type" {
  type = string
}

variable "worker_instance_name" {
  type = string
}

variable "worker_machine_type" {
  type = string
}

variable "region" {
  type = string
}

variable "zones" {
  type    = list(string)
  default = ["a", "b", "c"]
}

variable "subnet" {
  type = string
}

variable "external_ips" {
  type = list(string)
}

variable "tags" {
  type = list(string)
}

