resource "google_compute_network" "vpc" {
  name                    = var.vpc_name
  auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "vpc-subnet" {
  name          = var.rancher_subnet_name
  network       = google_compute_network.vpc.name
  ip_cidr_range = var.rancher_subnet_cidr_range
}

resource "google_compute_subnetwork" "vpc-subnet-2" {
  name          = var.kubernetes_subnet_name
  network       = google_compute_network.vpc.name
  ip_cidr_range = var.kubernetes_subnet_cidr_range
  region        = "us-west1"
}

resource "google_compute_firewall" "firewall_ingress" {
  name    = var.firewall_ingress_name
  network = google_compute_network.vpc.name

  allow {
    protocol = "udp"
    ports    = var.udp_ingress_ports
  }

  allow {
    protocol = "tcp"
    ports    = var.tcp_ingress_ports
  }

  target_tags = var.target_tags_ingress
}

resource "google_compute_firewall" "firewall_egress" {
  name    = var.firewall_egress_name
  network = google_compute_network.vpc.name

  allow {
    protocol = "all"
    ports    = []
  }

  direction = "EGRESS"

  target_tags = var.target_tags_egress
}

resource "google_compute_address" "external_ips_rancher" {
  count = 4
  name  = "rancher-${var.static_ips_name}-${count.index}"
}

resource "google_compute_address" "external_ips_kubernetes" {
  count  = 4
  name   = "kubernetes-${var.static_ips_name}-${count.index}"
  region = "us-west1"
}

