output "ips_rancher" {
  value = google_compute_address.external_ips_rancher.*.address
}

output "ips_kubernetes" {
  value = google_compute_address.external_ips_kubernetes.*.address
}



