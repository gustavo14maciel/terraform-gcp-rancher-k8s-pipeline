variable "vpc_name" {
  type = string
}
variable "rancher_subnet_name" {
  type = string
}
variable "kubernetes_subnet_name" {
  type = string
}
variable "rancher_subnet_cidr_range" {
  type = string
}
variable "kubernetes_subnet_cidr_range" {
  type = string
}
variable "firewall_ingress_name" {
  type = string
}
variable "udp_ingress_ports" {
  type = list(string)
}
variable "tcp_ingress_ports" {
  type = list(string)
}
variable "target_tags_ingress" {
  type = list(string)
}
variable "firewall_egress_name" {
  type = string
}
variable "target_tags_egress" {
  type = list(string)
}
variable "static_ips_name" {
  type = string
}

