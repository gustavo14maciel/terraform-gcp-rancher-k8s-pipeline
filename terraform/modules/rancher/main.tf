data "google_compute_image" "image" {
  family  = "ubuntu-pro-1604-lts"
  project = "ubuntu-os-pro-cloud"
}
data "google_service_account" "compute_engine_default_service_account" {
  account_id = "594525568521-compute@developer.gserviceaccount.com"
}

resource "google_compute_instance" "nginx" {
  name         = "nginx"
  machine_type = var.machine_type
  zone         = "${var.region}-${var.zones[0]}"

  boot_disk {
    initialize_params {
      image = data.google_compute_image.image.self_link
    }
  }

  network_interface {
    subnetwork = var.subnet

    access_config {
      nat_ip = var.external_ips[3]
    }
  }

  tags = var.tags

  service_account {
    email  = data.google_service_account.compute_engine_default_service_account.email
    scopes = ["cloud-platform"]
  }

  depends_on = [
    google_compute_instance.rancher_instances
  ]
}


resource "google_compute_instance" "rancher_instances" {
  count        = 3
  name         = "${var.rancher_instance_name}-${count.index}"
  machine_type = var.machine_type
  zone         = "${var.region}-${var.zones[count.index]}"

  boot_disk {
    initialize_params {
      image = data.google_compute_image.image.self_link
    }
  }

  network_interface {
    subnetwork = var.subnet

    access_config {
      nat_ip = var.external_ips[count.index]
    }
  }

  tags = var.tags

  service_account {
    email  = data.google_service_account.compute_engine_default_service_account.email
    scopes = ["cloud-platform"]
  }
}

data "template_file" "hosts" {
  template = file("../rancher/rancher-cluster.tpl")
  vars = {
    node_0_ip          = var.hosts[0]
    node_1_ip          = var.hosts[1]
    node_2_ip          = var.hosts[2]
    node_0_internal_ip = google_compute_instance.rancher_instances[0].network_interface.0.network_ip
    node_1_internal_ip = google_compute_instance.rancher_instances[1].network_interface.0.network_ip
    node_2_internal_ip = google_compute_instance.rancher_instances[2].network_interface.0.network_ip
    user               = var.user
  }
}

resource "local_file" "hosts" {
  content  = data.template_file.hosts.rendered
  filename = "../rancher/rancher-cluster.yml"
}

data "template_file" "nginx_config" {
  template = file("../nginx/nginx.conf.tpl")
  vars = {
    rancher_0 = var.hosts[0]
    rancher_1 = var.hosts[1]
    rancher_2 = var.hosts[2]
  }
}

resource "local_file" "nginx_config" {
  content  = data.template_file.nginx_config.rendered
  filename = "../nginx/nginx.conf"
}

