variable "rancher_instance_name" {
  type = string
}
variable "machine_type" {
  type = string
}
variable "subnet" {
  type = string
}
variable "external_ips" {
  type = list(string)
}
variable "tags" {
  type = list(string)
}
variable "zones" {
  type    = list(string)
  default = ["a", "b", "c"]
}
variable "region" {
  type = string
}
variable "hosts" {
  type = list(string)
}
variable "user" {
  default = "nindevops"
}

