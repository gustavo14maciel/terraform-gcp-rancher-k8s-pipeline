terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "4.4.0"
    }
  }
  backend "gcs" {
    bucket = "terraform-rancher-k8s"
    prefix = "terraform/state"
  }
}

provider "google" {
  alias = "impersonate"
  scopes = [
    "https://www.googleapis.com/auth/cloud-platform",
    "https://www.googleapis.com/auth/userinfo.email",
  ]
}

locals {
  terraform_service_account = "terraform@rancher-k8s-335122.iam.gserviceaccount.com"
}

data "google_service_account_access_token" "token" {
  provider               = google.impersonate
  target_service_account = local.terraform_service_account
  lifetime               = "1200s"

  scopes = [
    "https://www.googleapis.com/auth/cloud-platform",
  ]
}

provider "google" {
  project         = var.project_id
  access_token    = data.google_service_account_access_token.token.access_token
  request_timeout = "60s"
  region          = var.region
}
