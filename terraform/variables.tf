# MAIN MODULE
variable "project_id" {
  type = string
}
variable "region" {
  type = string
}

# NETWORK
variable "vpc_name" {
  type = string
}
variable "rancher_subnet_name" {
  type = string
}
variable "kubernetes_subnet_name" {
  type = string
}
variable "rancher_subnet_cidr_range" {
  type = string
}
variable "kubernetes_subnet_cidr_range" {
  type = string
}
variable "firewall_ingress_name" {
  type = string
}
variable "udp_ingress_ports" {
  type = list(string)

}
variable "tcp_ingress_ports" {
  type = list(string)
}
variable "target_tags_ingress" {
  type = list(string)
}
variable "firewall_egress_name" {
  type = string
}
variable "target_tags_egress" {
  type = list(string)
}
variable "static_ips_name" {
  type = string
}

# RANCHER
variable "rancher_instance_name" {
  type = string
}
variable "machine_type" {
  type = string
}

# DNS
variable "dns_zone_name" {
  type = string
}
variable "dns_name" {
  type = string
}
variable "instance_ip" {
  type = string
}

#KUBERNETES
variable "etcd_controlplane_instance_name" {
  type = string
}
variable "etcd_controlplane_machine_type" {
  type = string
}
variable "kubernetes_region" {
  type = string
}
variable "worker_etcd_instance_name" {
  type = string
}
variable "worker_etcd_machine_type" {
  type = string
}
variable "worker_instance_name" {
  type = string
}
variable "worker_machine_type" {
  type = string
}

